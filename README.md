# verify_key

It's an Arduino program to verify a password. 

You have 3 tries, if you fails led will be enlighted and if is the third time the buzzer will sound. If you hit the password then led green will be enlighted.

The schematic has been created with Fritzing:
https://fritzing.org/
