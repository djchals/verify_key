#include <LiquidCrystal.h>
#include <Keypad.h>

const byte ROWS=4, COLUMNS=4;
char keys[ROWS][COLUMNS]={
{'1','2','3','A'},
{'4','5','6','B'},
{'7','8','9','C'},
{'*','0','#','D'}
};

byte pinesRows[ROWS]={22,24,26,28};
byte pinesCols[COLUMNS]={30,32,34,36};

Keypad teclado=Keypad(makeKeymap(keys),pinesRows,pinesCols,ROWS,COLUMNS);

char KEY,PASS[7],MASTER_PASS[7]="1209";
byte INDEX=0;
const int LED_RED=10, LED_GREEN=9, BUZZER=8;
int num_tries=3;
LiquidCrystal lcd(7,6,5,4,3,2);

void setup() {
  lcd.begin(16,2);//this line indicates our lcd size 16cols, 2rows
  pinMode(LED_RED, OUTPUT); 
  pinMode(LED_GREEN, OUTPUT); 
  pinMode(BUZZER, OUTPUT); 
  
}

void loop() {
 lcd.setCursor(0,0);//col,fila
 lcd.print("Insert pass:");
  KEY=teclado.getKey();
  if(KEY){
    PASS[INDEX]=KEY;
    INDEX++;
    lcd.setCursor(INDEX,1);
    lcd.print("X");
    }
  if(INDEX==4){
    lcd.clear();
    //if the two strings are equals, srtrmp() returns 0, for this reason we invert the result with !
    if(!strcmp(PASS,MASTER_PASS)){
       digitalWrite(LED_GREEN,HIGH); //right!

      lcd.setCursor(0,0);
      lcd.print("Pass Accepted");
      delay(1000);
      lcd.clear();
      lcd.print("Welcome sr! :)");
      delay(1000);
      
    }else{
      lcd.setCursor(0,0);
      lcd.print("Pass Denied");
      digitalWrite(LED_RED,HIGH); 
      delay(1000);
      lcd.clear();
      lcd.setCursor(0,0);

      num_tries--;
      if(num_tries>0){
        lcd.print("You have ");
        lcd.setCursor(0,1);
        lcd.print(num_tries);
        lcd.print(" more tries");
      }else{
        num_tries=3;
        for(int i=0;i<5;i++){
          digitalWrite(LED_RED,HIGH); //ERROR!               
          lcd.print("Go out!");
          digitalWrite(BUZZER,HIGH);
          delay(200);
          digitalWrite(LED_RED,LOW); //ERROR!
          lcd.clear();                
          digitalWrite(BUZZER,LOW);
          delay(200);
        }

      }
    }
    delay(1000);
    INDEX=0;
    lcd.clear();
    digitalWrite(LED_GREEN,LOW); 
    digitalWrite(LED_RED,LOW); 
  }
}
